#field {
  display: grid;
 grid-template-columns: 20% 40% 40%;
}/nivel 1 completado

#field {
  display: grid;
  grid-template-columns: 50% 50%;  
} /nivel 2 completado

#field {
  display: grid;
  grid-template-columns: repeat(4, 25%);
  /* type here */
} /nivel 3 completado

#field {
  display: grid;
    grid-template-columns: repeat(3, 30%);
  /* type here */ 
} /nivel 4 completado

#field {
  display: grid;
  grid-template-columns: 100px 30%;
  /* type here */
}/nivel 5 completado

#field {
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  /* type here */ 
}/nivel 6 completado

#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  /* type here */ 
}/nivel 7 completado

#field {
  display: grid;
  grid-template-columns: 100px repeat(3, 1fr);
  /* type here */
}/nivel 8 completado

#field {
  display: grid;
  grid-template-columns: 20% 100px 1fr;
  /* type here */ 
}/nivel 9 completado

#field {
  display: grid;
  grid-template-columns: 1fr auto 1fr;
  /* type here */ 
}/nivel 10 completado

#field {
  display: grid;
    grid-template-columns: 25% 50% 25%;
  grid-template-rows: 100px 150px 1fr;
}  /nivel 11 completado

#field {
  display: grid;
    grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr 2fr 1fr;
}/nivel 12 completado

#field {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr 2fr 1fr;
column-gap: 15px
}/nivel 13 completado

#field {
  display: grid;
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr 2fr 1fr;
  column-gap: 5%
}/nivel 14 completado

#field {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr 2fr 1fr;
  row-gap: 40px
}/nivel 15 completado

#field {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr 2fr 1fr;
  column-gap: 10px;
  row-gap: 15%;
}/nivel 16 completado
#field {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: 1fr 2fr 1fr;
  gap: 20px;
}/nivel 17 completado

#field {
  display: grid;
    grid-template-columns: 1fr 100px auto;
  grid-template-rows: 1fr 1fr 100px;
  gap: 10% 20px;
}/nivel 18 completado

#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(2, 1fr);
}

#greenLand {
  grid-column-start: 3;
}/nivel 19 completado

#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(2, 1fr);
}

#greenLand {
  grid-column-start: 2;
  grid-column-end: 4;
}/nivel 20 completado

#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(2, 1fr);
  gap: 10px;
}

#greenLand {
  grid-column-start: span 2;
}/nivel 21 completado

#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: 100px 1fr;
  gap: 15px;
}

#greenLand {
  grid-column-start: span 2;
}/nivel 22 completado

#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(3, 1fr);
  gap: 10px;
}

#greenLand {
  grid-row-start: 2;
}/nivel 23 completado

#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: 10px;
}

#greenLand {
  grid-row-start: 1;
  grid-row-end: 5;
}/nivel 24 completado

#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: 10px;
}

#greenLand {
  grid-row-start: span 4;
}/nivel 25 completado

#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(3, 1fr);
  gap: 10px;
}

#redLand {
  grid-row-start: 1;
  grid-row-end: 4;
  grid-column-start: 1;
  grid-column-end: 3;
}/nivel 26 completado

#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: 15px;
}

#redLand {
  grid-row-start: 3;
  grid-row-end: 5;
  grid-column-start: 2;
  grid-column-end: 4;
}/nivel 27 completado

#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: 10px;
}

#greenLand {
  grid-column: 1 / 4;
  grid-row: 2 / 5;
}/nivel 28 completado


#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: 15px 10px;
}

#greenLand {
  grid-column: 2 / span 2;
  grid-row: 3 / 5;
}

#redLand {
  grid-column: 4;
  grid-row: 1 / 4;
}/nivel 29 completado

#field {
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(4, 1fr);
  gap: 15px;
  grid-template-areas: "land land land ."
      "land land land ."
      "land land land ."
      ". . . .";
}

#redLand {
  grid-area: land;
}/nivel 30 completado


#field {
  display: grid;
  grid-template-columns: 1fr 2fr 1fr;
  grid-template-rows: repeat(4, 1fr);
  gap: 15px;
  grid-template-areas: "redLand redLand greenLand"
      "redLand redLand greenLand"
      "redLand redLand greenLand"
      ". . greenLand";
}

#greenLand {
  grid-area: greenLand;
}

#redLand {
  grid-area: redLand;
}/nivel 31 completado

#field {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(3, 1fr);
  gap: 15px 10px;
  grid-template-areas: "greenLand greenLand ."
      "greenLand greenLand ."
      "redLand redLand redLand";
}

#greenLand {
  grid-area: greenLand;
}

#redLand {
  grid-area: redLand;
}/nivel 32 completado

#field {
  display: grid;
  grid-template-columns: min-content 1fr;
  grid-template-rows: 1fr 1fr;
}/nivel 33 completado

  display: grid;
  display: grid;
  grid-template-columns: min-content 1fr;
  grid-template-rows: 1fr min-content;  
}/nivel 34 completado

#field {
  display: grid;
  grid-template-columns: max-content 1fr;
  grid-template-rows: 1fr 1fr;
}/nivel 35 completado

#field {
  display: grid;
  grid-template-columns: max-content min-content;
  grid-template-rows: 1fr 1fr; 
}/nivel 36 completado

#field {
  display: grid;
  grid-template-columns: minmax(250px, 1fr) 1fr;
  grid-template-rows: 1fr 1fr;
  gap: 15px;
}/nivel 37 completado

#field {
  display: grid;
  grid-template-columns: minmax(min-content, 200px) 150px;
  grid-template-rows: 1fr 1fr;
}/nivel 38 completado

#field {
  display: grid;
  grid-template-columns: minmax(max-content, 200px) minmax(min-content, auto);
  grid-template-rows: 1fr 1fr;
}/nivel 39 completado

#field {
  display: grid;
  grid-template-columns: repeat(auto-fill, 150px);
  gap: 15px;
}/nivel 40 completado

#field {
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(150px, 1fr));
  gap: 15px;
}/nivel 41 completado

#field {
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(100px, 1fr));
  gap: 15px;
}/nivel 42 completado

#field {
  display: grid;
  grid-template: 100px 1fr 100px / 100px 1fr 100px;
  gap: 20px;
}/nivel 43 completado

#field {
  display: grid;
  gap: 15px;
  grid-template: "redLand redLand redLand"
      ". greenLand greenLand"
      ". greenLand greenLand";
}

#greenLand {
  grid-area: greenLand;
}

#redLand {
  grid-area: redLand;
}/nivel 44 completado

#field {
  display: grid;
  gap: 15px;
  grid-template: "redLand redLand ." 100px
      "blueLand greenLand greenLand" 200px
      "blueLand greenLand greenLand" 1fr / 1fr 1fr 1fr
}

#greenLand {
  grid-area: greenLand;
}

#redLand {
  grid-area: redLand;
}

#blueLand {
  grid-area: blueLand;
}/nivel 45 completado

#field {
  display: grid;
  grid-template: repeat(3, 1fr) / repeat(3, 1fr);
  gap: 20px;
  grid-auto-flow: column;
}

#greenLand {
  grid-column-start: 3;
}/nivel 46 completado

#field {
  display: grid;
  grid-template: repeat(4, 1fr) / repeat(4, 1fr);
  grid-auto-flow: dense;
  gap: 20px;
}

#field div:nth-child(3n) {
  grid-column: span 3;
}/nivel 47 completado

#field {
  display: grid;
  grid-template: repeat(4, 1fr) / repeat(4, 1fr);
  grid-auto-flow: column;
  gap: 15px;
}

#greenLand {
  grid-column: span 3;
}/nivel 48 completado

#field {
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-auto-columns: 1fr;
}

#redLand {
  grid-column: 3;
}/nivel 49 completado

#field {
  display: grid;
  gap: 20px;
  grid-template: 1fr 1fr / 1fr 1fr;
  grid-auto-rows: 100px;
}/nivel 50 completado

#field {
  display: grid;
  gap: 20px;
  grid-template: 1fr 1fr / 1fr 1fr;
  grid-auto-columns: 100px;
  grid-auto-rows: 100px;
}

#redLand {
  grid-column: 3;
  grid-row: 3;
}/nivel 51 completado

#field {
  display: grid;
  gap: 15px;
  grid-template: 1fr 1fr / 1fr 1fr;
  grid-auto-columns: 1fr;
  grid-auto-rows: 100px 150px;
  grid-auto-flow: column;
}

#redLand {
  grid-column: 3;
  grid-row: 4;
}/nivel 52 completado

#field {
  display: grid;
  gap: 15px;
  grid-template-columns: repeat(3, 1fr);
  grid-auto-rows: 100px;
  justify-items: start;
}

#field > div {
  width: 75%;
}/nivel 53 completado

#field {
  display: grid;
  gap: 15px;
  grid-template-columns: repeat(2, 1fr);
  grid-auto-rows: 100px;
  justify-items: end;
}

#field > div {
  width: 70%;
}/nivel 54 completado

#field {
  display: grid;
  grid-template: 100px 1fr / 2fr 1fr;
  justify-items: center;
}

#field > div {
  width: 50%;
}/nivel 55 completado

#field {
  display: grid;
  gap: 15px;
  grid-template: repeat(3, 1fr) / repeat(3, 1fr);
  align-items: start;
}

#field > div {
  height: 50%;
}/nivel 56 completado

#field {
  display: grid;
  grid-template: repeat(3, 1fr) / repeat(2, 1fr);
  align-items: end;
}

#field > div {
  height: 50%;
}/nivel 57 completado

#field {
  display: grid;
  grid-template: 2fr 1fr / repeat(3, 1fr);
  align-items: center;
}

#field > div {
  height: 50%;
}/nivel 58 completado

#field {
  display: grid;
  grid-template: 1fr 1fr / 2fr 1fr 1fr;
  align-items: center;
  justify-items: center;
}

#field > div {
  height: 50%;
  width: 50%;
}

#greenLand {
  grid-column: span 2;
}/nivel 59 completado

#field {
  display: grid;
  gap: 15px;
  grid-template-columns: repeat(3, 1fr);
  place-items: center end;
}

#field > div {
  height: 50%;
  width: 50%;
}/nivel 60 completado

#field {
  display: grid;
  gap: 15px;
  grid-template: 1fr 1fr / 1fr 1fr;
  justify-items: end;
}

#field > div {
  height: 50%;
  width: 50%;
}

#redLand {
  justify-self: start;
}/nivel 61 completado

#field {
  display: grid;
  gap: 15px;
  grid-template: 1fr 1fr / 1fr 1fr;
}

#redLand {
  height: 50%;
  width: 50%;
  justify-self: center;
}/nivel 62 completado

#field {
  display: grid;
  grid-template: 1fr 1fr / 200px 1fr;
  gap: 15px;
}

.redLand {
  width: 50%;
  height: 50%;
  justify-self: end;
}/nivel 63 completado

#field {
  display: grid;
  grid-template: 1fr 2fr / 1fr 2fr;
  gap: 15px;
}

.redLand {
  width: 100%;
  height: 50%;
  align-self: center;
}/nivel 64 completado

#field {
  display: grid;
  grid-template: 1fr 1fr 1fr / 1fr 1fr 1fr;
}

.redLand {
  width: 100%;
  height: 50%;
  align-self: end;
}/nivel 65 completado

#field {
  display: grid;
  gap: 15px;
  grid-template: 1fr 1fr / 2fr 1fr;
  justify-items: center;
  align-items: center;
}

#field > div {
  height: 50%;
  width: 50%;
}

#redLand {
  justify-self: end;
  align-self: end;
}

#greenLand {
  justify-self: start;
  align-self: end;
}/nivel 66 completado

#field {
  display: grid;
  grid-template: 1fr 1fr 1fr / 2fr 1fr;
  place-items: start;
}

#field > div {
  height: 50%;
  width: 50%;
}

#redLand {
  place-self: center end;
}

#greenLand {
  place-self: start end;
}/nivel 67 completado

#field {
  display: grid;
  gap: 15px;
  grid-template-columns: 150px 150px;
  justify-content: center;
}/nivel 68 completado

#field {
  display: grid;
  gap: 15px;
  grid-template-columns: 30% 30%;
  justify-content: end;
}/nivel 69 completado

#field {
  display: grid;
  grid-template: 1fr 1fr / 40% 40%;
  justify-content: space-evenly;
  justify-items: end;
}

#field > div {
  width: 50%;
  height: 50%;
}/nivel 70 completado

#field {
  display: grid;
  gap: 15px;
  grid-template-columns: 1fr 1fr;
  grid-auto-rows: 100px;
  align-content: center;
}/nivel 71 completado

#field {
  display: grid;
  grid-template-columns: 40% 40%;
  grid-auto-rows: 125px;
  align-content: end;
}/nivel 72 completado

#field {
  display: grid;
  grid-template-columns: 1fr 100px;
  grid-auto-rows: 100px;
  align-content: space-between;
}/nivel 73 completado

#field {
  display: grid;
  grid-template: repeat(4, 1fr) / repeat(4, 1fr);
  gap: 15px;
}

#greenLand {
  grid-row: 2 / 5;
  grid-column: 2 / 4;
}/nivel 74 completado

#field {
  display: grid;
  grid-template: repeat(3, 1fr) / repeat(3, 1fr);
  gap: 15px 10px;
  grid-template-areas: "greenLand greenLand blueLand"
      "greenLand greenLand blueLand"
      "redLand redLand redLand";
}

#greenLand {
  grid-area: greenLand;
}

#redLand {
  grid-area: redLand;
}

#blueLand {
  grid-area: blueLand;
}/nivel 75 completado

#field {
  display: grid;
  grid-template-columns: minmax(max-content, 1fr) minmax(min-content, auto);
  grid-template-rows: 1fr 1fr;
}/nivel 76 completado

#field {
  display: grid;
  gap: 15px;
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-auto-rows: 100px;
  align-content: space-between;
}

#field div:nth-child(3n) {
  grid-column: span 3;
}/nivel 77 completado

#field {
  display: grid;
  gap: 15px;
  grid-template: "greenLand . blueLand" 1fr
      "greenLand redLand ." 1fr
      / 100px 1fr 1fr;
}

#greenLand {
  grid-area: greenLand;
}

#redLand {
  grid-area: redLand;
}

#blueLand {
  grid-area: blueLand;
  height: 50%;
  align-self: center;
}/nivel 78 completado

#field {
  display: grid;
  gap: 15px;
  grid-template: repeat(4, 1fr);
}

#greenLand {
  grid-column: 2 / span 3;
  grid-row: 3 / 5;
}

#redLand {
  grid-column: 3 / span 2;
  grid-row: 1 / 4;
}/nivel 79 completado

#field {
  display: grid;
  grid: repeat(3, 1fr);
}

#greenLand {
  grid-column: 1 / 3;
  grid-row: 1;
}

#redLand {
  grid-column: 2 / 4;
  grid-row: 1 / 3;
}

#blueLand {
  grid-column: 2;
  grid-row: 1 / 5;
}/nivel 80 completado
